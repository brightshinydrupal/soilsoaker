#ifndef Relay_h
#define Relay_h

/**
 * Controls operation of a relay.
 */
class Relay {
 protected:
   int relay_pin = -1;

 public:
   Relay();
   void relayPin(int relay_motor_pin);
   void start();
   void stop();
};

#endif
