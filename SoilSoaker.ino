/*
  Plant soil moisture controller.
  
  REMEMBER TO DISCONNECT D0 <-> RST when updating code.
  Stores sensor values on Thingspeak.

   
- NodeMCU V2.0
- Thingspeak data storage
- NodeMcu and Mux Pinout
#    D0 <-> RST - Enable Deep Sleep 
    A0 <-> Soil sensor
    D1 <-> Relay powering the water pump
*/

// Project definitions
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "ThingSpeak.h"
#include "soil.h"
#include "communications.h"
#include "relay.h"
#include "oled.h"
 
WiFiClient  client;
SoilSensor soil;
Relay relay;

unsigned long sleepSeconds = 900;
unsigned int wateringSeconds = 5;

void napTime() {
  return pretendNapTime();
  
  Serial.println("Taking a nap for " + String(sleepSeconds) + " seconds");
  ESP.deepSleep(sleepSeconds * 1e6, WAKE_RF_DEFAULT);  
}

/**
 * Simulate a deep sleep, updating the soil % display while waiting to "Wake".
 */
void pretendNapTime() {
  char soil_text[100];
  char sleeping_text[100];

  const int loop_delay_seconds = 3;
  
  for (int countdown = sleepSeconds; countdown > 0; countdown -= loop_delay_seconds) {
    Serial.println("Nap over in " + String(countdown) + " seconds");

    int soil_moisture = soil.value();

    sprintf(soil_text, "Soil %d%%", soil_moisture);
    sprintf(sleeping_text, "Wake in %d sec", countdown);
    drawText("Moisture", soil_text, sleeping_text);

    delay(loop_delay_seconds * 1000);
  }

}



void setup() {
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);  

  if (!connect_wifi()) {
    napTime();
  }

  soil.sensorPin(A0);
  relay.relayPin(D1);
  oled_setup();
}


void run_update_loop() {
  char soil_text[100];
  
  ThingSpeak.begin(client);
      
  digitalWrite(LED_BUILTIN, LOW);  // Turn the LED on. 

  int soil_moisture = soil.value();

  sprintf(soil_text, "Soil %d%%", soil_moisture);

  char serial_update[100];
  sprintf(serial_update,"Soil=%d rssi=%d\n", 
    soil_moisture,
    get_rssi()
  );
  Serial.println(serial_update);

  if (soil_moisture < 0) {
    drawText("Moisture", soil_text, "Watering!");
    
    Serial.println("Watering for 5 seconds");
    relay.start();
    delay(wateringSeconds * 1000);
    relay.stop();
  }
  drawText("Moisture", soil_text);

  // Send data to Thingspeak for storage.
  thingspeaksenddata(
    soil_moisture,
    int(soil_moisture < 0),
    get_rssi()
  );
}


void loop() {
  run_update_loop();
  digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off. 
  napTime();
}
