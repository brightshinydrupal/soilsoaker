#include "soil.h";
#include <Arduino.h>

SoilSensor::SoilSensor() {}

void SoilSensor::sensorPin(int pin)
{
  pinMode(pin, INPUT);
  _pin = pin;
}

// An arbitrary value of 1-100.
int SoilSensor::value(int max) {
  int output_value = analogRead(_pin);
  output_value = map(output_value,550,0,0,100);
  Serial.print("Mositure : ");
  Serial.print(output_value);
  Serial.println("%");
  return output_value;
}

int SoilSensor::value() {
  return this->value(100);
}
