/************************************* 
 *  setup wifi communications Nodemcu
 *************************************/

#include "credentials.h"
 
// local network
const char * ssid = credentials_ssid;
const char * pass = credentials_pass;

// Thingspeak credentials
unsigned long myChannelNumber = credentials_myChannelNumber;
const char * myWriteAPIKey = credentials_myWriteAPIKey;

bool connect_wifi()
{
  Serial.print("Attempting to connect to SSID: ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);

  // Loops for no more than 10 * (.25 + .25) = 10 seconds.
  int loop_count = 0;
  while((WiFi.status() != WL_CONNECTED) && (loop_count++ < 20)) {
    digitalWrite(LED_BUILTIN, LOW);  // Flutter the LED on while we try to connect. 
    Serial.print("*");
    delay(250);
    digitalWrite(LED_BUILTIN, HIGH);  // Flutter the LED on while we try to connect.
    delay(250);
  }
  if (loop_count >= 20) {
    Serial.println("\nFailed to connect, will try again later.");    
    return false;
  }
  
  Serial.println("\nConnected.");
  delay(1000);
  return true;
}


long get_rssi() {
    return WiFi.RSSI();
}

/*********************************** 
 *  Send data to Thingspeak.
 ***********************************/
 void thingspeaksenddata(int soil_moisture, int watered, int rssi){
  Serial.println("thingspeaksenddata started");       
  ThingSpeak.setField(1,float(soil_moisture)); 
  ThingSpeak.setField(2,float(watered)); 
  ThingSpeak.setField(3,float(rssi)); 
  int result = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  Serial.print("thingspeaksenddata completed, result was");       
  Serial.println(result);       
}
