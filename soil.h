#ifndef SoilSensor_h
#define SoilSensor_h

class SoilSensor {
  public:
    SoilSensor();
    void sensorPin(int pin);
    int value();  // Uses max = 100;
    int value(int max);
  private:
    int _pin;
};

#endif
