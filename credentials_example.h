/*
 * Credentials include file
 * 
 * Copy this to credentials.h and supply real values.
 */
 
// local network
char credentials_ssid[] = "MySSID";
char credentials_pass[] = "SuperSecretWifiPassword";

// Thingspeak credentials
unsigned long credentials_myChannelNumber = 0000001;
const char * credentials_myWriteAPIKey = "SuperSecretThingspeakApiKey";
