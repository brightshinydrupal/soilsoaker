#include <SPI.h>

#include "relay.h"


Relay::Relay() {}

/**
 * Set up the pin attached to the external motor relay.
 */
void Relay::relayPin(int relay_motor_pin) {
  relay_pin = relay_motor_pin;  
  pinMode(this->relay_pin, OUTPUT);
  this->stop(); 
}

void Relay::start() {
  digitalWrite(this->relay_pin, HIGH);    
}

void Relay::stop() {
  digitalWrite(this->relay_pin, LOW);    
}
