#include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"`

SSD1306Wire  display(0x3c, D3, D2);  //D3=SDA, D2=SCL  As per labeling on NodeMCU

/**
 * Initialize the display.
 */
void oled_setup() {
  display.init();
}

/**
 * Print text to the OLED display.
 * 
 * Title is printed larger.
 * Text is printed next.
 * text2, if provided, is printed yellow.
 * 
 * create more fonts at http://oleddisplay.squix.ch/
 */
void drawText(const char * title, const char * text, const char * text2 = "") {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_24);  
  display.drawString(0, 0, title);
  display.setFont(ArialMT_Plain_16);
  display.drawString(0, 28, text);
  display.drawString(0, 46, text2);
  display.display();  
}  
